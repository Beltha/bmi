/*
 *  Author: Tamas Molnar
 *  email: motsaatsze91@gmail.com
 *  www.facebook.com/Beltha91
 *  Epam WebDev- Bootcamp 2017-Oct 
 *  Instructor: Richard Pal
 *  
 *  First Java homework - BMI Counter
 * 
 */

package bmi;

import java.util.*;

public class BmiBritish {
	
	int feet, inches, heightInInches;
	double BMI, weightInPounds;
	static final int BMI_MULTIPLIER = 703;
	Scanner valueScanner = new Scanner(System.in);
	
	public BmiBritish() {
		feet = 1;
		inches = 8;
		setHeightInInches(feet, inches);
		weightInPounds = 5;
	}
	
	public BmiBritish(int feet, int inches, double pounds) {
		this.feet = feet;
		this.inches = inches;
		this.heightInInches = (this.feet * 12) + this.inches;
		this.weightInPounds = pounds;
	}
	
	
	public double IUSCount(int feet, int inches, double pounds) {
		
		System.out.println("Enter your weight in pounds:");
		if (!valueScanner.hasNextDouble()) {
			throw new IllegalArgumentException("Weight must be a number!");
		}
		weightInPounds = valueScanner.nextDouble();
		if (weightInPounds < 5 || weightInPounds > 660) {
			throw new IllegalArgumentException("Weight must be a number from 5 to 660!");
		}
		
		System.out.println("Enter your height:");
		System.out.println("Feet:");
		if (!valueScanner.hasNextInt()) {
			throw new IllegalArgumentException("Height must be a number!"); 
		}
		
		feet = valueScanner.nextInt();
		if (feet < 1 || feet > 8) {
			throw new IllegalArgumentException("Feet must be a number from 1 to 8!");
		}
		
		System.out.println("Inches:");
		inches = valueScanner.nextInt();
		if (inches < 0 || inches > 12) {
			throw new IllegalArgumentException("Inches must be a number from 0 to 12!");
		}
		setHeightInInches(feet, inches);
		if (heightInInches < 20 || heightInInches > 98) {
			throw new IllegalArgumentException("Height must be a number from 1 feet 8 inches to 8 feet 2 inches!");
		}
		
		valueScanner.close();
		BMI = (weightInPounds * BMI_MULTIPLIER) / (heightInInches * heightInInches);
		return BMI;
	}

	public int getFeet() {
		return feet;
	}

	public void setFeet(int feet) {
		this.feet = feet;
	}

	public int getInches() {
		return inches;
	}

	public void setInches(int inches) {
		this.inches = inches;
	}

	public int getHeightInInches() {
		return heightInInches;
	}

	public void setHeightInInches(int feet, int inches) {
		this.heightInInches = (feet*12) +inches;
	}
}

