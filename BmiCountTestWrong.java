package bmi;

import org.junit.Assert;
import org.junit.Test;

public class BmiCountTestWrong {

	@Test
	public void CountTestWrong() {
		BmiDecimal test = new BmiDecimal();
		double result = test.SICount(101, 200); // result=25.25
		Assert.assertEquals(25,result,0);
	}

}
