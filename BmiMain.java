/*
  *  Author: Tamas Molnar
 *  email: motsaatsze91@gmail.com
 *  www.facebook.com/Beltha91
 *  Epam WebDev- Bootcamp 2017-Oct 
 *  Instructor: Richard Pal
 *  
 *  First Java homework - BMI Counter
 * 
 *  The program counts the user's BMI from input data.
 *  It could be in decimal and British system too. You can choose from command line.
 *  The program also gives a little information about your health: 
 *  -if your index is below 18.5, the message will be: "You are underweighted!"
 *  -if your index is between 18.5-25.0, the message will be: "Your weight is normal!"
 *  -if your index is over 25.0, the message will be: "You are overweighted!"
 *  
 *  The program handles wrong data of input fields:
 *  -It will have given exception if the user gives too high or too low numbers
 *  	->weight should be between 2.25kg-300kg
 *  	->weight should be between 5 pounds-660 pounds
 *  	->height should be between 50 cm-250cm
 *  	->height should be between 1 feet 8 inches - 8 feet 2 inches
 *  -It will have given exception if the user gives other characters instead of numbers
 *  
 *  Few JUnit tests also have been created to test some methods.
 */

package bmi;

import java.util.Scanner;

public class BmiMain {

public static void main(String[] args) {
		
		double finalBmi; 
		Scanner menuScanner = new Scanner(System.in);
		
		BmiBritish bmib = new BmiBritish();
		BmiDecimal bmid = new BmiDecimal();
		
		System.out.println("Choose menu '1' if you want count in Imperial Unit System(feet, inches, pounds):");
		System.out.println("Choose menu '2' if you want count in International System of Units (meters, kilograms):");
		System.out.println("Choose menu '3' if you want to quit:");

		int choice = menuScanner.nextInt();
		
			switch (choice) {
			
			case 1:
				finalBmi = bmib.IUSCount(bmib.feet, bmib.inches, bmib.weightInPounds);	//count in British system
				System.out.println("Your BMI is:" + finalBmi);
				System.out.println(Advice(finalBmi));
				break;
			
			case 2:
				finalBmi = bmid.SICount(bmid.weight, bmid.height);		//count in Decimal system
				System.out.println("Your BMI is:" + finalBmi);
				System.out.println(Advice(finalBmi));
				break;
				
			case 3:
				menuScanner.close();
				return;
				
			default: 
				System.out.println("You gave wrong number!");
				break;
			}
			menuScanner.close();
	}
	
		public static String Advice(double bmi) {
			String advice = "";
			if (bmi <= 18.5) {
				advice = "You are underweight!"; 
			} else if (bmi > 18.5 && bmi < 25) {
				advice = "Your weight is normal!";
			} else { 
				advice = "You are overweight!";
			}
			return advice;
		}
}
