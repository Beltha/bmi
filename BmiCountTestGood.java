package bmi;

import org.junit.Assert;
import org.junit.Test;

public class BmiCountTestGood {

	@Test
	public void CountTestGood() {
		BmiDecimal test = new BmiDecimal();
		double result = test.SICount(100, 200);
		Assert.assertEquals(25,result,0);		// (100/(200*200/10000))=25
	}
}
