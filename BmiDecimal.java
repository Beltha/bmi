/*
 *  Author: Tamas Molnar
 *  email: motsaatsze91@gmail.com
 *  www.facebook.com/Beltha91
 *  Epam WebDev- Bootcamp 2017-Oct 
 *  Instructor: Richard Pal
 *  
 *  First Java homework - BMI Counter
 * 
 */

package bmi;

import java.util.Scanner;

public class BmiDecimal {
	
	double BMI, weight, height;
	Scanner valueScanner = new Scanner(System.in);

	public BmiDecimal() {
	
		weight = 2.25;
		height = 0.5;
	}
	
	public BmiDecimal(double weight, double height) {
		this.weight = weight;
		this.height = height;
	}
	
	//count in Decimal system
	public double SICount(double weight, double height) {
		
		
		System.out.println("Enter your weight in kilograms:");
		if (!valueScanner.hasNextDouble()) {
			throw new IllegalArgumentException("Weight must be a number!");
		}
		weight = valueScanner.nextDouble();
		if (weight<2.25 || weight > 300) {
			throw new IllegalArgumentException("Weight must be a number from 2.25 to 300!");
		}
		
		System.out.println("Enter your height in centimeters:");
		if (!valueScanner.hasNextDouble()) {
			throw new IllegalArgumentException("Height must be a number!"); 
		}
		height = valueScanner.nextDouble() / 100;
		if (height<0.5 || height > 2.5) {
			throw new IllegalArgumentException("Height must be a number from 50 to 250!");
		}
		
		valueScanner.close();
		BMI = weight / (height * height);		
		return BMI;	
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
}

